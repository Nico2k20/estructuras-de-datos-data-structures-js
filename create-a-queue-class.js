function Queue() {
  var collection = [];
  this.print = function() {
    console.log(collection);
  };
  // Only change code below this line
  this.enqueue= function(elem){
    collection.unshift(elem);
  }
  this.dequeue= function(){
    let last_element= collection[collection.length-1];
    collection.pop()
    return last_element;
  }
  this.front = function(){
    return collection[collection.length-1];
  }
  this.size = function(){
    return collection.length;
  }
  this.isEmpty= function(){
    return collection.length==0;
  }

  // Only change code above this line
}

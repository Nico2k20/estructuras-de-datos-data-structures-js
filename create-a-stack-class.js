function Stack() {
  var collection = [];
  this.print = function() {
    console.log(collection);
  };
  // Only change code below this line
  this.push = function(elem){
    collection.push(elem)
  }
  this.pop = function(){
    let last_element=collection[collection.length-1];
    collection.pop();
    return last_element;
  }
  this.peek = function(){
    let last_element=collection[collection.length-1];
    return last_element;
  }
  this.isEmpty = function(){
    return collection.length==0;
  }
  this.clear=function(){
    collection =[] 
  }

  // Only change code above this line
}
 
